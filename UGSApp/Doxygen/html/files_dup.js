var files_dup =
[
    [ "crc16.cpp", "d7/dd9/crc16_8cpp.html", "d7/dd9/crc16_8cpp" ],
    [ "crc16.h", "d5/d7c/crc16_8h.html", "d5/d7c/crc16_8h" ],
    [ "DataRoutines.cpp", "de/d1c/_data_routines_8cpp.html", "de/d1c/_data_routines_8cpp" ],
    [ "DataRoutines.h", "db/d19/_data_routines_8h.html", "db/d19/_data_routines_8h" ],
    [ "EventLog.cpp", "d7/d42/_event_log_8cpp.html", null ],
    [ "EventLog.h", "d5/d99/_event_log_8h.html", [
      [ "EventLog", "d4/d57/class_event_log.html", "d4/d57/class_event_log" ]
    ] ],
    [ "evlogmsg.h", "d2/db6/evlogmsg_8h.html", "d2/db6/evlogmsg_8h" ],
    [ "NTServApp.cpp", "d6/d5d/_n_t_serv_app_8cpp.html", "d6/d5d/_n_t_serv_app_8cpp" ],
    [ "NTServApp.h", "dc/dde/_n_t_serv_app_8h.html", "dc/dde/_n_t_serv_app_8h" ],
    [ "NTService.cpp", "d4/d24/_n_t_service_8cpp.html", "d4/d24/_n_t_service_8cpp" ],
    [ "NTService.h", "d7/d66/_n_t_service_8h.html", "d7/d66/_n_t_service_8h" ],
    [ "Resource.h", "d8/db4/_resource_8h.html", "d8/db4/_resource_8h" ],
    [ "Settings.cpp", "d6/dcd/_settings_8cpp.html", "d6/dcd/_settings_8cpp" ],
    [ "Settings.h", "d4/d4a/_settings_8h.html", "d4/d4a/_settings_8h" ],
    [ "stdafx.cpp", "df/d9d/stdafx_8cpp.html", "df/d9d/stdafx_8cpp" ],
    [ "stdafx.h", "db/d06/stdafx_8h.html", "db/d06/stdafx_8h" ],
    [ "UGS.cpp", "d4/d9d/_u_g_s_8cpp.html", "d4/d9d/_u_g_s_8cpp" ],
    [ "UGS.h", "db/dbb/_u_g_s_8h.html", "db/dbb/_u_g_s_8h" ],
    [ "ugsservice.cpp", "d5/d29/ugsservice_8cpp.html", null ],
    [ "ugsservice.h", "d3/d76/ugsservice_8h.html", [
      [ "CUGSService", "d7/def/class_c_u_g_s_service.html", "d7/def/class_c_u_g_s_service" ]
    ] ]
];