var searchData=
[
  ['m_5faroutprefix_246',['m_arOutPrefix',['../dc/d11/class_c_settings.html#a6fd29e09f70974766ae33c3d9afafdcb',1,'CSettings']]],
  ['m_5farprefix_247',['m_arPrefix',['../dc/d11/class_c_settings.html#add1ddc72bb2c35beacd7a66adfde9205',1,'CSettings']]],
  ['m_5farstatusdata_248',['m_arStatusData',['../dc/d11/class_c_settings.html#ada9030207850727851d08f78f67c22a8',1,'CSettings']]],
  ['m_5farstatusmsg_249',['m_arStatusMsg',['../dc/d11/class_c_settings.html#aa4543404c0a91bd0e0ad304578265362',1,'CSettings']]],
  ['m_5fbisrunning_250',['m_bIsRunning',['../d4/db7/class_c_n_t_service.html#a4f6f4fdf701805b9da0adcfd47fe2a5d',1,'CNTService']]],
  ['m_5fbkeeplog_251',['m_bKeepLog',['../dc/d11/class_c_settings.html#a1645dc391471286ecddcc445e9b81917',1,'CSettings']]],
  ['m_5fblogpackall_252',['m_bLogPackAll',['../dc/d11/class_c_settings.html#a29bc4bb853fa77e25aa6ee549491f297',1,'CSettings']]],
  ['m_5fblogunpackall_253',['m_bLogUnpackAll',['../dc/d11/class_c_settings.html#a7d0924f9cd15e50ef5ba9283a9d77b1d',1,'CSettings']]],
  ['m_5fbmarkall_254',['m_bMarkAll',['../dc/d11/class_c_settings.html#ac567cf036f41c68ea938cd992484901d',1,'CSettings']]],
  ['m_5fbshowcomerrors_255',['m_bShowCOMErrors',['../dc/d11/class_c_settings.html#a0dae1f61ebb1acef51dbe3da7f7095db',1,'CSettings']]],
  ['m_5fbshowmessageerrors_256',['m_bShowMessageErrors',['../dc/d11/class_c_settings.html#a7171e054047b4acf5869ea99163a0520',1,'CSettings']]],
  ['m_5fbshowsiomessages_257',['m_bShowSIOMessages',['../dc/d11/class_c_settings.html#a994fceab6d26672d93a438357d82ffc1',1,'CSettings']]],
  ['m_5fbtestloopback_258',['m_bTestLoopback',['../dc/d11/class_c_settings.html#a1bf43e581a4d33d0d07940a3839f13d0',1,'CSettings']]],
  ['m_5fbunpackall_259',['m_bUnpackAll',['../dc/d11/class_c_settings.html#a2a8a7fb7ac60ae0c7f0c333166f369c2',1,'CSettings']]],
  ['m_5fdwpollingperiod_260',['m_dwPollingPeriod',['../dc/d11/class_c_settings.html#afb21fbe1a7067a71c775d7752add2906',1,'CSettings']]],
  ['m_5fheventsource_261',['m_hEventSource',['../d4/db7/class_c_n_t_service.html#a2d5fc1ef218d79a0c0b6e323f149fc14',1,'CNTService']]],
  ['m_5fhservicestatus_262',['m_hServiceStatus',['../d4/db7/class_c_n_t_service.html#a641374d68f6869326014c22b27aa3348',1,'CNTService']]],
  ['m_5ficomrit_263',['m_iCOMRit',['../dc/d11/class_c_settings.html#aa71525a8c7762d4e3d7bcd18e3a2f1dd',1,'CSettings']]],
  ['m_5ficomrttc_264',['m_iCOMRttc',['../dc/d11/class_c_settings.html#a542bf3aee88f9ecb7cc4afe6987f9ccc',1,'CSettings']]],
  ['m_5ficomwttc_265',['m_iCOMWttc',['../dc/d11/class_c_settings.html#ad1b130beeb25344d0689b7a6f96fbc9c',1,'CSettings']]],
  ['m_5fisendstatto_266',['m_iSendStatTO',['../dc/d11/class_c_settings.html#a2e3b20001450dbd555e8e05f1ec476b4',1,'CSettings']]],
  ['m_5fistartparam_267',['m_iStartParam',['../d7/def/class_c_u_g_s_service.html#a93f547862373541283e02fe1f428c5d1',1,'CUGSService']]],
  ['m_5fistate_268',['m_iState',['../d7/def/class_c_u_g_s_service.html#a1e138022ed7e060d7fc335f2de074e69',1,'CUGSService']]],
  ['m_5fmaplogmsgtypestopack_269',['m_mapLogMsgTypesToPack',['../dc/d11/class_c_settings.html#a89d17aee86c37f4b0649781610263856',1,'CSettings']]],
  ['m_5fmaplogmsgtypestounpack_270',['m_mapLogMsgTypesToUnpack',['../dc/d11/class_c_settings.html#a46b3e6d4354aff2882d0f9dba0c66f3b',1,'CSettings']]],
  ['m_5fmapmsgtypes_271',['m_mapMsgTypes',['../dc/d11/class_c_settings.html#aa512e6228ca7e56df49f91f4fd7ab1a5',1,'CSettings']]],
  ['m_5fmapmsgtypestomark_272',['m_mapMsgTypesToMark',['../dc/d11/class_c_settings.html#af61403d2b635ad9f0e759116c039b254',1,'CSettings']]],
  ['m_5fmapmsgtypestounpack_273',['m_mapMsgTypesToUnpack',['../dc/d11/class_c_settings.html#a6994476133febf6f1c578b058f44d765',1,'CSettings']]],
  ['m_5fmarkcomposedmask_274',['m_MarkComposedMask',['../dc/d11/class_c_settings.html#a5a45050826f09e52af27f8793e464c20',1,'CSettings']]],
  ['m_5fmarknestedmask_275',['m_MarkNestedMask',['../dc/d11/class_c_settings.html#af62d815e093febebbd92222522bcba1d',1,'CSettings']]],
  ['m_5fnbuffersize_276',['m_nBufferSize',['../dc/d11/class_c_settings.html#a419ed14a807b0235f9cbfefd1d4e272a',1,'CSettings']]],
  ['m_5fnincomingport_277',['m_nIncomingPort',['../dc/d11/class_c_settings.html#a0734200c3dd577a62399670d56797805',1,'CSettings']]],
  ['m_5fnstatusperiod_278',['m_nStatusPeriod',['../dc/d11/class_c_settings.html#ad67f77d51600137f801db07be4e72f2a',1,'CSettings']]],
  ['m_5fpthis_279',['m_pThis',['../d4/db7/class_c_n_t_service.html#a5c81eaa18ba3fafb3a54ef587573bb0c',1,'CNTService']]],
  ['m_5fstatus_280',['m_Status',['../d4/db7/class_c_n_t_service.html#a97e0829a00beee2ebc1faf39e09c6ef7',1,'CNTService']]],
  ['m_5fstatushdr_281',['m_StatusHdr',['../dc/d11/class_c_settings.html#a6002fa901d714b38465b455b53ff7360',1,'CSettings']]],
  ['m_5fstatusmsg_282',['m_StatusMsg',['../dc/d11/class_c_settings.html#a82dc88f759aa6d5c6ab348f6f69b6c19',1,'CSettings']]],
  ['m_5fstrcomsetup_283',['m_strCOMSetup',['../dc/d11/class_c_settings.html#ab9bba9907e554fd17b88043253f02d59',1,'CSettings']]],
  ['m_5fstrincomingaddress_284',['m_strIncomingAddress',['../dc/d11/class_c_settings.html#ad4b26eed349af71e01c92296cfc78e5e',1,'CSettings']]],
  ['m_5fstrsettingsreportpath_285',['m_strSettingsReportPath',['../dc/d11/class_c_settings.html#a1354809a3d4748ffddb7706b6d522157',1,'CSettings']]],
  ['m_5fszservicename_286',['m_szServiceName',['../d4/db7/class_c_n_t_service.html#a8b098a5cbdda62084d9f4b8f1060888c',1,'CNTService']]],
  ['m_5fszworkingdir_287',['m_szWorkingDir',['../d4/db7/class_c_n_t_service.html#a68d436ba147752fe45e0df6783bd79b1',1,'CNTService']]],
  ['m_5ftuprimtosecsrc_288',['m_TUPrimToSecSrc',['../dc/d11/class_c_settings.html#a416e91a2bea3b246f937bb1d7a8d732e',1,'CSettings']]],
  ['m_5ftusectoprimsrc_289',['m_TUSecToPrimSrc',['../dc/d11/class_c_settings.html#af12c46c418dd3fe97664c72d67333ddb',1,'CSettings']]],
  ['m_5ftusrccommsgindex_290',['m_TUSrcComMsgIndex',['../dc/d11/class_c_settings.html#a1c3937f00d53e295e2f9beb509c08d68',1,'CSettings']]],
  ['m_5ftusrcmask_291',['m_TUSrcMask',['../dc/d11/class_c_settings.html#aaba9fac6508fdadbf7ce4b1ba88405dd',1,'CSettings']]],
  ['m_5ftutype_292',['m_TUType',['../dc/d11/class_c_settings.html#ab6ec0d1ddf99dab2cb6632edaae22843',1,'CSettings']]],
  ['m_5fwcomposedtype_293',['m_wComposedType',['../dc/d11/class_c_settings.html#a3f43c099ec864bb6461a0e50dc2af4da',1,'CSettings']]],
  ['m_5fwcpaddr_294',['m_wCPAddr',['../dc/d11/class_c_settings.html#aebb2bd19b5f4334d3aeedefc510dea21',1,'CSettings']]],
  ['m_5fwcrc16init_295',['m_wCRC16Init',['../dc/d11/class_c_settings.html#aa41377e96200fd1065282327a43fa718',1,'CSettings']]],
  ['m_5fwdestination_296',['m_wDestination',['../db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#a0ec4697f786f7002ab4889852e506ab4',1,'CSettings::tagMESSAGETYPE']]],
  ['m_5fwdestmask_297',['m_wDestMask',['../db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#abea68706fd9440bc02529bb1a8ef52b5',1,'CSettings::tagMESSAGETYPE']]],
  ['m_5fwlogcomposedtype_298',['m_wLogComposedType',['../dc/d11/class_c_settings.html#a6ee6e84d47432e0f21edc51fea26a03c',1,'CSettings']]],
  ['m_5fwlogcomposedtypetopack_299',['m_wLogComposedTypeToPack',['../dc/d11/class_c_settings.html#afb16d72f52815d0b707734c2ae975764',1,'CSettings']]],
  ['m_5fwmaxlength_300',['m_wMaxLength',['../db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#aec11defcfcd67d1339bb434106bd6004',1,'CSettings::tagMESSAGETYPE']]],
  ['m_5fwoutputcomposedtype_301',['m_wOutputComposedType',['../dc/d11/class_c_settings.html#a78ac25b6683e0e8487dd908be7d7cfdc',1,'CSettings']]],
  ['m_5fwpuaddr_302',['m_wPUAddr',['../dc/d11/class_c_settings.html#a706fb30ae796b700a549d2ee3966ffca',1,'CSettings']]],
  ['m_5fwsource_303',['m_wSource',['../db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#ac6329646c5a6c2f9f55ea43d8e788dfc',1,'CSettings::tagMESSAGETYPE']]],
  ['m_5fwsourceid_304',['m_wSourceID',['../dc/d11/class_c_settings.html#ad30e2e0d1d0af777d697dde78f1efcbe',1,'CSettings']]],
  ['m_5fwsrcmask_305',['m_wSrcMask',['../db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#a5355cd3ad732f78acee9c1ec7d5e9696',1,'CSettings::tagMESSAGETYPE']]],
  ['m_5fwstatusrequestmessagetype_306',['m_wStatusRequestMessageType',['../dc/d11/class_c_settings.html#a6f70f7465c4b741f204154175c1f3866',1,'CSettings']]],
  ['m_5fwtype_307',['m_wType',['../db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#a8e27b00cdbaef1dc90e32c8da12f7d96',1,'CSettings::tagMESSAGETYPE']]]
];
