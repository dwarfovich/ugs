var searchData=
[
  ['save_137',['Save',['../dc/d11/class_c_settings.html#ab6c9da50cb19e763795634cdcaa4d8a5',1,'CSettings']]],
  ['savestatus_138',['SaveStatus',['../d7/def/class_c_u_g_s_service.html#a344dadd917e2903268672e20b37a5834',1,'CUGSService']]],
  ['service_5fcontrol_5fuser_139',['SERVICE_CONTROL_USER',['../d7/d66/_n_t_service_8h.html#a9029f8d9918fa095f580ac7f5df1fcdb',1,'NTService.h']]],
  ['servicemain_140',['ServiceMain',['../d4/db7/class_c_n_t_service.html#a88ff72785cbda7ae9ca686cf45d59c7a',1,'CNTService']]],
  ['setstatus_141',['SetStatus',['../d4/db7/class_c_n_t_service.html#a5ae39b12af1f29b9b07784d175981e51',1,'CNTService']]],
  ['settings_2ecpp_142',['Settings.cpp',['../d6/dcd/_settings_8cpp.html',1,'']]],
  ['settings_2eh_143',['Settings.h',['../d4/d4a/_settings_8h.html',1,'']]],
  ['setworkingdir_144',['SetWorkingDir',['../d4/db7/class_c_n_t_service.html#a8253c09ebfe95c11ba66b8739d7270ee',1,'CNTService']]],
  ['shellsort_145',['ShellSort',['../de/d1c/_data_routines_8cpp.html#a777f25c9fce00038ed0a2d5e7c258b2a',1,'ShellSort(void *pData, int iCount, size_t Size, int(__cdecl *pfnCompare)(const void *elem1, const void *elem2)):&#160;DataRoutines.cpp'],['../db/d19/_data_routines_8h.html#a777f25c9fce00038ed0a2d5e7c258b2a',1,'ShellSort(void *pData, int iCount, size_t Size, int(__cdecl *pfnCompare)(const void *elem1, const void *elem2)):&#160;DataRoutines.cpp']]],
  ['startservice_146',['StartService',['../d4/db7/class_c_n_t_service.html#aaed26d314afb625d8439ed36078ceed9',1,'CNTService']]],
  ['status_5fobject_5fname_5fnot_5ffound_147',['STATUS_OBJECT_NAME_NOT_FOUND',['../d4/d24/_n_t_service_8cpp.html#aebf1b984664178e02c74ee9fee793b68',1,'NTService.cpp']]],
  ['stdafx_2ecpp_148',['stdafx.cpp',['../df/d9d/stdafx_8cpp.html',1,'']]],
  ['stdafx_2eh_149',['stdafx.h',['../db/d06/stdafx_8h.html',1,'']]]
];
