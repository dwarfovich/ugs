var struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e =
[
    [ "tagMESSAGETYPE", "db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#a24efcd5556f7437729b8b448bb405576", null ],
    [ "m_wDestination", "db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#a0ec4697f786f7002ab4889852e506ab4", null ],
    [ "m_wDestMask", "db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#abea68706fd9440bc02529bb1a8ef52b5", null ],
    [ "m_wMaxLength", "db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#aec11defcfcd67d1339bb434106bd6004", null ],
    [ "m_wSource", "db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#ac6329646c5a6c2f9f55ea43d8e788dfc", null ],
    [ "m_wSrcMask", "db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#a5355cd3ad732f78acee9c1ec7d5e9696", null ],
    [ "m_wType", "db/dd6/struct_c_settings_1_1tag_m_e_s_s_a_g_e_t_y_p_e.html#a8e27b00cdbaef1dc90e32c8da12f7d96", null ]
];